package scribbles.lib.functions

import java.math.BigDecimal
import java.math.MathContext
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * A utility for measuring execution time in some simple CLI apps.
 * todo maybe not "logging time", but nothing? and then remove the dash, unless message is specified
 */
inline fun <T> logTime(message: String = "logging time", startLabel: String = "start: ", endLabel: String = "end:   ", pattern: String = "HH:mm:ss.SSS", block: () -> T): T {
    val formatter = DateTimeFormatter.ofPattern(pattern)
    val start = LocalDateTime.now()
    println("$message - $startLabel${start.format(formatter)}")
    val result = block()
    val end = LocalDateTime.now()
    val duration = Duration.between(start, end)
    println("$message - $endLabel${end.format(formatter)}, duration: ${duration.toMillis().toDouble() / 1000} seconds")
    return result
}

//usage
fun main() {
    logTime("calculating stuff") {
        var x = BigDecimal(0)
        repeat(999999) {
            x += BigDecimal("999999").sqrt(MathContext.DECIMAL128)
        }
        println("x = $x")
    }
}