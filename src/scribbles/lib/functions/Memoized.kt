package scribbles.lib.functions

/**
 * CAUTION:
 * - use only for pure functions
 * - type [T] should be a valid map key
 */
fun <T, R> memoized(function: (T) -> R): (T) -> R {
    val cache = mutableMapOf<T, R>()
    return fun(param: T): R {
        val maybeResult = cache[param]
        return if (maybeResult != null) {
            maybeResult
        } else {
            val result = function(param)
            cache[param] = result
            result
        }
    }
}

// todo overload for (T, U) -> R, etc., but you get the idea

fun main() {
    fun negative(x: Int): Int {
        println("Actually called for param value: $x")
        return -x
    }
    val memoizedNegative = memoized(::negative)
    println(memoizedNegative(5))
    println(memoizedNegative(3))
    println(memoizedNegative(3))
    println(memoizedNegative(4))
    println(memoizedNegative(5))
    println(memoizedNegative(4))
    println(memoizedNegative(3))
    println(memoizedNegative(5))
    println(memoizedNegative(4))
}