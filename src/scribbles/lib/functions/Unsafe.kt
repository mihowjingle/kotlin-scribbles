package scribbles.lib.functions

sealed class Unsafe<T> {
    data class Success<U>(val body: U) : Unsafe<U>()
    data class Failure<U>(val exception: Exception) : Unsafe<U>()
}

/**
 * "Guards" the [action] by returning a [Unsafe.Success] of the action's expected result [R], or [Unsafe.Failure] of whatever exception occurred.
 * Use when you have reason to suspect the attempted action might fail, but you want to do something about that.
 * todo and... you hate try/catch? will this be useful?
 * (Especially using Java libraries, which rely heavily on checked exceptions, and which you have no control over,
 * although it would probably be better to write a specifically tailored wrapper)
 *
 * (no, nevermind - you can only catch one exception: [Exception],
 * specific type is lost, no catching generics, etc)
 */
inline fun <R> unsafe(action: () -> R): Unsafe<R> {
    return try {
        val result = action()
        Unsafe.Success(result)
    } catch (exception: Exception) {
        Unsafe.Failure(exception)
    }
}

///////////////////////////// EXAMPLE SETUP: /////////////////////////////

fun addTwo(x: Int): Int = throw IllegalStateException()
fun addThree(x: Int) = x + 3

fun main() {
    val a = unsafe { addTwo(3) }
    println("a = $a")
    if (a is Unsafe.Success) {
        println("a.body = ${a.body}")
    } else if (a is Unsafe.Failure) {
        println("a.exception = ${a.exception}")
    }

    val b = unsafe { addThree(3) }
    println("b = $b")
    when (b) {
        is Unsafe.Success -> {
            println("b.body + 7 = ${b.body + 7}")
        }
        is Unsafe.Failure -> {
            println("b.exception = ${b.exception}")
        }
    }
}