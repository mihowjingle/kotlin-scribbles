package scribbles.lib.functions

/**
 * For reducing (some) boilerplate when you want to force a user
 * to input data in some specific format in a CLI app, or otherwise validate it etc.
 *
 * So why not? It forces you to use labeled return (negative scenario) and that, apparently, is a bad practice.
 * And of course questionable utility in the first place, as in most cases here.
 */
inline fun <R> eventuallyNotNull(block: () -> R?): R {
    var result: R?
    do {
        result = block()
    } while (result == null)
    return result
}

// usage
fun main() {
    val sum = eventuallyNotNull {
        print("Please supply numbers in x, x, x format -> ")
        val input = readLine() ?: return@eventuallyNotNull null
        try {
            input.split(", ").sumOf { it.toInt() }
        } catch (_: NumberFormatException) {
            null
        }
    }

    println("The sum of the numbers is: $sum")
}