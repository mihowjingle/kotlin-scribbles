package scribbles.lib.functions

typealias Action = () -> Unit

inline fun forever(action: Action): Nothing {
    while (true) {
        action()
    }
}