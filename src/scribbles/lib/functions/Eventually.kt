package scribbles.lib.functions

class Eventually {

    fun <R> doTry(block: Eventually.() -> R): R {
        while (true) {
            try {
                return block()
            } catch (_: Retry) {
                // continue
            }
        }
    }

    class Retry : Exception(null, null, false, false)

    fun retry(): Nothing = throw Retry()
}

/**
 * For reducing (some) boilerplate when you want to force a user
 * to input data in some specific format in a CLI app, or otherwise validate it etc.
 *
 * To be used in conjunction with [Eventually.retry],
 * which becomes available inside the function/lambda passed to this function.
 */
fun <R> eventually(block: Eventually.() -> R): R {
    return Eventually().doTry(block)
}

// usage
fun main() {
    val sum = eventually {
        print("Please supply numbers in x, x, x format -> ")
        val input = readLine() ?: retry()
        try {
            input.split(", ").sumOf { it.toInt() }
        } catch (_: NumberFormatException) {
            retry()
        }
    }

    println("The sum of the numbers is: $sum")
}