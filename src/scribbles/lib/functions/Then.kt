package scribbles.lib.functions

fun <T, R, U> compose(first: (T) -> R, second: (R) -> U): (T) -> U {
    return fun(argument) = second(first(argument))
}

// better:

/**
 * Compose [this] (receiver) function with the [next].
 *
 * The resulting function is equivalent to [this] and then [next] applied sequentially.
 */
infix fun <T, R, U> ((T) -> R).then(next: (R) -> U): (T) -> U {
    return fun(argument) = next(this(argument))
}

// btw, to make it more readable, pretty much invoke, but infix
infix fun <T, R> ((T) -> R).applyTo(arg: T) = this(arg)

// ...and the reverse, probably a bit much (not more readable/convenient, but weird)
infix fun <T, R> T.processedWith(function: (T) -> R) = function(this)

// usage
fun main() {

    fun plusFive(x: Int) = x + 5
    fun timesTwo(x: Int) = x * 2

    val shouldBeTen = (::plusFive then ::timesTwo)(0)
    println("Should be ten: $shouldBeTen")

    val shouldBeFive = compose(::timesTwo, ::plusFive)(0)
    println("Should be five: $shouldBeFive")

    val shouldBeTenAlso = ::plusFive then ::timesTwo applyTo 0
    println("Should be ten: $shouldBeTenAlso")

    val shouldBeFiveAlso = compose(::timesTwo, ::plusFive) applyTo 0
    println("Should be five: $shouldBeFiveAlso")

    val shouldBeTenToo = 0 processedWith (::plusFive then ::timesTwo)
    println("Should be ten: $shouldBeTenToo")

    val shouldBeFiveToo = 0 processedWith compose(::timesTwo, ::plusFive)
    println("Should be five: $shouldBeFiveToo")

    /////////////////////////////////////

    fun minusThree(x: Int) = x - 3
    fun square(x: Int) = x * x

    // advantage of "then"
    val somethingLonger: (Int) -> Unit = ::plusFive then ::timesTwo then ::minusThree then ::square then ::println

    // same thing with "compose", ugh
    val somethingLongerOmgPleaseNo: (Int) -> Unit = compose(compose(compose(compose(::plusFive, ::timesTwo), ::minusThree), ::square), ::println)

    // yeah, we can make it step-by-step with intermediate variables... lots of code and not much more readable, if at all
    val addFiveThenTimesTwo = compose(::plusFive, ::timesTwo)
    val addFiveThenTimesTwoThenSubtractThree = compose(addFiveThenTimesTwo, ::minusThree)
    val addFiveThenDoubleThenSubtractThreeThenSquare = compose(addFiveThenTimesTwoThenSubtractThree, ::square)
    val somethingLongerNotMuchBetter = compose(addFiveThenDoubleThenSubtractThreeThenSquare, ::println)

    ///////////////////////////////////// just playing for the sake of it, at this point

    fun <T> chainThenPrintln(vararg functions: (T) -> T): (T) -> Unit {
        var chain: (T) -> T = { x -> x } // a.k.a identity
        for (function in functions) {
            chain = chain then function
        }
        return chain then ::println
    }

    chainThenPrintln(::plusFive, ::timesTwo, ::minusThree, ::square)(5)
    chainThenPrintln<String>({ it + "e" }, { it + "llo" })("H")
}