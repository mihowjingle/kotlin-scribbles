package scribbles.lib.numbers

import scribbles.lib.functions.logTime

fun Long.isPrime(): Boolean {
    if (this == 1L) {
        return false
    }
    var down = 2L
    var up = this
    while (down < up) {
        if (this % down == 0L) {
            return false
        }
        up = this / down
        down++
    }
    return true
}

fun Int.isPrime(): Boolean {
    if (this == 1) {
        return false
    }
    var down = 2
    var up = this
    while (down < up) {
        if (this % down == 0) {
            return false
        }
        up = this / down
        down++
    }
    return true
}

fun main() {
    logTime {
        var sum = 0L
        (1..100000L).associateWith { it.isPrime() }.forEach { (number, isPrime) ->
            if (isPrime) {
                sum += number
            }
        }
        println(sum)
    }
    logTime {
        var sum = 0
        (1..100000).associateWith { it.isPrime() }.forEach { (number, isPrime) ->
            if (isPrime) {
                sum += number
            }
        }
        println(sum)
    }
}