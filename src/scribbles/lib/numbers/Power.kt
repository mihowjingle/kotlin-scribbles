package scribbles.lib.numbers

/**
 * Good old power, seems like a silly gap for Kotlin not to have power for Ints, only Floats and Doubles,
 * in its standard library. Do I not know something?
 *
 * WARNING: This function only supports non-negative exponents!
 *
 * @throws IllegalArgumentException for negative [x]
 */
infix fun Int.power(x: Int): Int {
    if (x < 0) {
        throw IllegalArgumentException("This function only supports non-negative exponents!") // at least for now
    }
    if (x == 0 || this == 1) {
        return 1
    }
    if (this == 0) {
        return 0
    }
    var accumulator = 1
    repeat(times = x) {
        accumulator *= this
    }
    return accumulator
}

// todo fraction exponent? negative exponent?

fun main() {
    println(-2 power 3)
    println(-2 power 2)
    println(0 power 3)
    println(1 power 7)
    println(2 power 3)
    println(3 power 3)
    println(5 power 2)
    println(7 power 3)
    println(7 power 2)
    println(7 power 1)
    println(7 power 0)
}