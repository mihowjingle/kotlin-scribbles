package scribbles.lib.numbers

fun Int.clamp(min: Int, max: Int): Int {
    return when {
        this < min -> min
        this > max -> max
        else -> this
    }
}

fun main() {
    val numbers = 1..10
    val clamped = numbers.map { it.clamp(3, 8) }
    println(clamped)
}