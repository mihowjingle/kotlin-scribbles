package scribbles.lib.iterables

import kotlin.math.max
import kotlin.math.min

// todo test
// todo alternate name? more descriptive one... UnorderedPair?
// todo is it even needed? (you can always use a Set)
data class Association<out A, out B>(val a: A, val b: B) {
    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other !is Association<*, *>) {
            return false
        }
        return a == other.a && b == other.b || a == other.b && b == other.a
    }

    override fun hashCode(): Int {
        val aCode = a.hashCode()
        val bCode = b.hashCode()
        val maxCode = max(aCode, bCode)
        val minCode = min(aCode, bCode)
        return maxCode + 37 * minCode
    }

    override fun toString(): String {
        return "Association($a <-> $b)"
    }
}