package scribbles.lib.iterables

fun <K, V, R> Map<K, Iterable<V>>.foldValues(initial: R, operation: (R, V) -> R): Map<K, R> {
    return this.mapValues { (_, values) -> values.fold(initial, operation) }
}

// todo why the inheritance?
fun <K, V : R, R> Map<K, Iterable<V>>.reduceValues(operation: (R, V) -> R): Map<K, R> {
    return this.mapValues { (_, values) -> values.reduce(operation) }
}

// todo maybe copy and rewrite from scratch?
fun <S, T : S> Iterable<T>.optionalReduce(operation: (S, T) -> S): S? {
    return try {
        this.reduce(operation)
    } catch (_: UnsupportedOperationException) {
        return null
    }
}

fun <K, V : R, R> Map<K, Iterable<V>>.optionalReduceValues(operation: (R, V) -> R): Map<K, R?> {
    return this.mapValues { (_, values) -> values.optionalReduce(operation) }
}

// todo beautiful candidate for lib-fun: but does it make sense? also seems kinda poorly performant
// also, it seems as if it is needed only to clean up
// after a bad groupBy (the case where I came up with the idea) - just fix your groupBy then?

/**
 * Traverses the receiver Map finding all pairs of keys which satisfy the [keysEquivalent] predicate.
 * When such a pair of keys is found, it applies the "merging" [operation] to the values
 * and that merged value lands in the resulting Map, associated to either of the [K]s (since they are deemed equivalent).
 * Merging continues until no more pairs of [K] satisfying the [keysEquivalent] predicate can be found.
 */
fun <K, V> Map<K, V>.merge(keysEquivalent: (K, K) -> Boolean, operation: (V, V) -> V): Map<K, V> {
    TODO()
}

// todo indexOfMaxBy, indexOfMinBy? averageBy! ...maybe more like averageOf, to my lib?