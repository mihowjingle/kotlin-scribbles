package scribbles.lib.iterables

import java.util.*

fun <X, Y> cartesian(one: Iterable<X>, other: Iterable<Y>): List<Pair<X, Y>> {
    val pairs = LinkedList<Pair<X, Y>>()
    for (x in one) {
        for (y in other) {
            pairs.add(Pair(x, y))
        }
    }
    return pairs
}

operator fun <X, Y> Iterable<X>.times(that: Iterable<Y>): List<Pair<X, Y>> = cartesian(this, that)

fun <X, Y> cartesian(one: Collection<X>, other: Collection<Y>): List<Pair<X, Y>> {
    val pairs = ArrayList<Pair<X, Y>>(one.size * other.size) // is it worth it?
    for (x in one) {
        for (y in other) {
            pairs.add(Pair(x, y))
        }
    }
    return pairs
}

operator fun <X, Y> Collection<X>.times(that: Collection<Y>): List<Pair<X, Y>> = cartesian(this, that)

fun <X, Y, Z> cartesian(one: Iterable<X>, other: Iterable<Y>, another: Iterable<Z>): List<Triple<X, Y, Z>> {
    val triples = LinkedList<Triple<X, Y, Z>>()
    for (x in one) {
        for (y in other) {
            for (z in another) {
                triples.add(Triple(x, y, z))
            }
        }
    }
    return triples
}

// ... and so on

fun main() {
    val min = 1
    val max = 5
    val numbers = min..max // .toSet(), .toList()... any Iterable is fine
    println("Multiplication table from $min to $max, inclusive:")
    cartesian(numbers, numbers)
            .associateWith { (x, y) -> x * y }
            .forEach { (arguments, result) -> println("${arguments.first} * ${arguments.second} = $result") }

    println("\n=-=-=-=-=-=-=-=-=-=-=-=-=\n")

    val cities = "Wrocław, Lublin, Poznań, Warszawa, Rzeszów".split(", ")
    println("All possible one-way trips between: $cities")
    (cities * cities)
            .filter { (left, right) -> left != right } // Lublin -> Lublin is not much of a trip
            .forEach { (left, right) -> println("$left -> $right") }
}