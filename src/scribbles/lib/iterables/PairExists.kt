package scribbles.lib.iterables

// can't make the receiver of this function an Iterable and then for (element in this) { ... }
// and check if it is the exact same object based on === (identity), because of JVM optimization for small integers
// (they will actually be the same object, even if they logically shouldn't be)
// ... also string cache, I guess?
inline fun <T> Array<T>.pairExists(predicate: (T, T) -> Boolean): Boolean { // todo "anyTwo"? "anyPair"? ...or just "any" (overload)
    for (x in this.indices) {
        for (y in this.indices) {
            if (x == y) continue // exact same object
            if (predicate(this[x], this[y])) {
                return true
            }
        }
    }
    return false
}

inline fun <reified T> Collection<T>.pairExists(predicate: (T, T) -> Boolean): Boolean {
    return this.toTypedArray().pairExists(predicate)
}

fun main() {
    val numbers = listOf(1, 3, 5, 4, 2, 4)
    val thereAreTwoOrMoreEqualNumbers = numbers.pairExists { x, y -> x == y }
    println("$numbers contains duplicate -> $thereAreTwoOrMoreEqualNumbers")

    val names = setOf("Tomas", "Mike", "Adele", "Barbara")
    val someFirstSomeLast = names.pairExists { name1, name2 -> name1.first().equals(name2.last(), ignoreCase = true) }
    println("Someone's name in $names ends with the same letter, " +
            "with which someone else's name starts (case-ignored) -> $someFirstSomeLast") // yes, A<-dele vs Barbar->a
}