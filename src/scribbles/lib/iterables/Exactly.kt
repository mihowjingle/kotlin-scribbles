package scribbles.lib.iterables

// todo check if this is indeed more performant
//  (possibly very early return vs additional check every time)
//  in either case, the function can still be kept
//  as syntactic sugar for .count { predicate } == occurrences
inline fun <T> Iterable<T>.exactly(occurrences: Int, predicate: (T) -> Boolean): Boolean {
    var count = 0
    for (element in this) {
        if (predicate(element)) {
            count++
            if (count > occurrences) {
                return false
            }
        }
    }
    return count == occurrences
}

fun main() {
    val numbers = listOf(1, 2, 2, 4, 2)
    val exactlyThreeTwos = numbers.exactly(occurrences = 3) { it == 2 }
    println("Are there exactly three 2's in $numbers? $exactlyThreeTwos")
}