package scribbles.lib.iterables

/**
 * ... well, basically this.filter(predicate).max()
 *
 * ... so, not really needed I guess?
 */
fun <T : Comparable<T>> Iterable<T>.maxWhere(predicate: (T) -> Boolean): T? {
    val iterator = this.iterator()
    if (!iterator.hasNext()) {
        return null
    }
    var max = iterator.next()
    while (iterator.hasNext()) {
        val current = iterator.next()
        if (current > max && predicate(current)) {
            max = current
        }
    }
    return max
}

fun main() {
    val shouldBe7 = listOf(1, 2, 5, 6, 18, 22, 7).maxWhere { it < 10 }
    println(shouldBe7)
    val shouldBeNull = listOf<Int>().maxWhere { it != 0 }
    println(shouldBeNull)
}