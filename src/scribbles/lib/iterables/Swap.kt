package scribbles.lib.iterables

fun <T> MutableList<T>.swap(i1: Int, i2: Int) { // todo good, useful, etc? too trivial for a library?
    val temp = this[i1]
    this[i1] = this[i2]
    this[i2] = temp
}

fun main() {
    val names = mutableListOf("Tom", "Jerry", "Filemon")
    names.swap(1, 2)
    println(names)
}