package scribbles.lib.booleans

/**
 * todo
 */
fun <T> Boolean.translate(ifTrue: T, ifFalse: T) = if (this) ifTrue else ifFalse

/**
 * Cool, but maybe a bit confusing (you don't usually "invoke" a Boolean)
 */
operator fun <T> Boolean.invoke(ifTrue: T, ifFalse: T) = translate(ifTrue = ifTrue, ifFalse = ifFalse)

// usage
fun main() {

    print("Please provide a number: ")

    val x = readln().toInt()

    val even = x % 2 == 0

    println("$x is ${even(ifTrue = "even", ifFalse = "odd")}")

    fun positive(a: Int) = a > 0

    println("$x is ${positive(x).translate(ifTrue = "positive", ifFalse = "negative or zero")}")
}