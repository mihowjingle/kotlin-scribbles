package scribbles.lib.booleans

/**
 * Self-explanatory. A little better looking.
 */
fun neither(a: Boolean, b: Boolean) = !a && !b

fun main() {
    val number = readln().toInt()
    if (neither(number > 0, number < 0)) {
        println("Must be zero!")
    }
}