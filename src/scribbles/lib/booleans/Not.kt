package scribbles.lib.booleans

/**
 * For visibility - a single exclamation mark can blend in very well :) todo worth it?
 */
fun not(boolean: Boolean) = !boolean