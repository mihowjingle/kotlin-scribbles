package scribbles.lib.strings

/**
 * This is like the ?: operator, except it also checks if the receiver String is empty.
 */
infix fun String?.otherwise(that: String) = if (this != null && this.isNotEmpty()) this else that

val String.quoted get() = "\"$this\"" // omg, Kotlin :D

fun main() {
    val possiblyEmpty = "" // or null
    val notEmpty = possiblyEmpty otherwise "default"
    println("notEmpty = ${notEmpty.quoted}")
}