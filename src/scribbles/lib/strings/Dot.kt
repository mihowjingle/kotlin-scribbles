package scribbles.lib.strings

// as (almost) always - useful (enough)?

infix fun String.dot(other: String) = "$this.$other"
// or even...
infix fun Any.dot(other: Any) = "${this}.$other"

fun main() {
    val path = "group" dot "category" dot "item" dot "property" // for jpa criteria or something
    val sillyDate = 2020 dot 11 dot 27
    println("path = $path")
    println("sillyDate = $sillyDate")
}
