package scribbles.lib.strings

// todo rightPad(ded)ToLongest
// todo any Iterable or Collection of Strings?

fun MutableList<String>.leftPadToLongest(padWith: Char = ' ') {
    val maxLength = this.maxOf { it.length }
    for (idx in this.indices) {
        val string = this[idx]
        this[idx] = string.padStart(maxLength, padWith)
    }
}

fun List<String>.leftPaddedToLongest(padWith: Char = ' '): List<String> {
    if (this.isEmpty()) {
        return this
    }
    val result = mutableListOf<String>()
    val maxLength = this.maxOf { it.length }
    for (string in this) {
        result.add(string.padStart(maxLength, padWith))
    }
    return result
}

fun main() {
    val list = mutableListOf("erggfd", "45gdscvttx", "66hy")
    list.leftPadToLongest()
    println(list)
    println(listOf("erggfd", "45gdscvttx", "66hy").leftPaddedToLongest('_'))
}