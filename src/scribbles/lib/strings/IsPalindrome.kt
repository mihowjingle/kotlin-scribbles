package scribbles.lib.strings

import scribbles.lib.booleans.translate

// todo nice, fun exercise, but is it actually useful?
//  maybe more fitting for a lesson in my future kotlin tutorial?

fun String.isPalindrome(): Boolean {
    val caseIgnored = this.toLowerCase()
    var left = 0
    var right = caseIgnored.lastIndex
    while (left < right) {
        if (caseIgnored[left] != caseIgnored[right]) {
            return false
        }
        left++
        right--
    }
    return true
}

fun main() {
    println("abbba".isPalindrome().translate(ifTrue = "palindrome", ifFalse = "not palindrome"))
    println("abbcba".isPalindrome().translate(ifTrue = "yes", ifFalse = "no"))
}