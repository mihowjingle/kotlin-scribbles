package scribbles.lib.strings

// todo optional parameter 'ignoreCase'?

/**
 * Turns all fragments of consecutive [char]s into just one [char], for example:
 *
 * @return "abba" for receiver "aabbaaa" and [char] 'a'
 */
fun String.collapse(char: Char): String {
    var result = ""
    var alreadyEncounteredThisTime = false
    for (character in this) {
        if (character != char) {
            alreadyEncounteredThisTime = false
            result += character
            continue
        } // character == char:
        if (alreadyEncounteredThisTime) {
            continue
        } // character == char and not alreadyEncounteredThisTime:
        result += char
        alreadyEncounteredThisTime = true
    }
    return result
}

fun present(test: String, argument: Char) {
    println("\"$test\".collapse('$argument') -> \"${test.collapse(argument)}\"")
}

fun main() { // todo actual tests
    present("aabbbaa", 'a')
    present("aabbba", 'a')
    present("aabbb", 'a')
    present("aabbbaa", 'a')
    present("baa", 'a')
    present("aabbbaa", 'b')
    present("aabbbaa", 'c')
    present("aabbbaazzzzzzzzz", 'z')
    present("aabxxbxxbaa", 'x')
    present("a", 'a')
    present("a", 'b')
    present("", 'x')
}
