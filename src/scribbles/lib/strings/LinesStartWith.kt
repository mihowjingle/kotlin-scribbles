package scribbles.lib.strings

/**
 * To make string blocks less annoying.
 */

// todo disallow empty lines? (completely, without even [char])
// | aaaaaa
//
// | bbbbbbb
// todo disallow varying indent of [char] itself? like:
// | aaaaaaaa
//       | bbbb
// instead only allow this?:
// | aaaaaaaa
// | bbbb
fun String.linesStartWith(char: Char): String {
    val noWhitespaceLines = lines().map { it.trimStart() }.filterNot { it.isEmpty() }
    for (line in noWhitespaceLines) {
        val actualChar = line[0]
        if (actualChar != char) {
            throw IllegalArgumentException("Unexpected character - $actualChar instead of $char - at the beginning of a line!")
        }
    }
    return noWhitespaceLines.joinToString("\n") { it.drop(1) }
}

fun main() {

    val properlyIndentedString = """
        |//some code
        |fun add(x: Int, y: Int): Int {
        |    return x + y
        |}
        |
        | 1
        |  2
        |   3
        |    4
        | dfvbvcbcvb
        |                     sdfdsfsdfsddfs
        |                asadas
    """.linesStartWith('|')

    println("-------------------")
    println(properlyIndentedString)
    println("-------------------")
}