package scribbles.other

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class Reaction(val command: String, val description: String, val action: () -> Unit)

fun reaction(command: String, description: String, action: () -> Unit) = Reaction(command, description, action)

val reactions = listOf(
    reaction(command = "hello", description = "Says hello back.") {
        println("Hello there!")
    },
    reaction(command = "time", description = "Prints current time in HH:mm:ss format.") {
        println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")))
    },
    reaction(command = "date", description = "Prints current date in yyyy-MM-dd format.") {
        println(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
    },
    reaction(command = "datetime", description = "Prints current date and time in yyyy-MM-dd HH:mm:ss format.") {
        println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
    },
    reaction(command = "help", description = "Prints all available commands.") {
        printAllCommands()
    },
    reaction(command = "coin", description = "Prints \"heads\" or \"tails\" with 50/50 chance.") {
        println(listOf("heads", "tails").random())
    },
    reaction(command = "exit", description = "Exits the program.") {
        println("See you!") // ... or it could just be empty
    }
)

// IDEAS FOR THE FUTURE:
// multiple commands per (re)action? hello + hi, exit + quit, etc
// pad-to-longest the command(s) in "help page" so that the arrow is always in the same position
// action consumes the input as in "() -> Unit" --> "(String) -> Unit" (especially needed for parsing math expressions and whatnot)

fun printAllCommands() {
    reactions
        .map { reaction -> "${reaction.command} -> ${reaction.description}" }
        .forEach { println(it) }
}

val defaultReaction = reaction("", "") {
    println("Command not recognized, type in \"help\" for the list of available commands.")
}

fun main() {
    println("Welcome to the super hyper mega turbo question answerer 2000 a.k.a. Reactor!")
    do {
        print("> ")
        val command = readln()
        val reaction = reactions.find { it.command == command } ?: defaultReaction
        reaction.action()
    } while (reaction.command != "exit")
}