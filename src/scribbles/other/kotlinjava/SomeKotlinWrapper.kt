package scribbles.other.kotlinjava

class SomeKotlinWrapper {

    private val someJavaClass = SomeJavaClass()

    fun someNotNullMethod(): String = someJavaClass.someNotNullMethod() // because we checked
    fun someNullableMethod(): String? = someJavaClass.someNullableMethod() // because we checked
    fun someOptionalMethod(): String? = someJavaClass.someOptionalMethod().orElseGet { null } // because we checked (and it's kind of obvious)
}