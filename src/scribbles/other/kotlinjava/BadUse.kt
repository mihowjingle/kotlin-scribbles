package scribbles.other.kotlinjava

fun main() {
    val someJavaClass = SomeJavaClass()
    println(someJavaClass.someNotNullMethod().length) // ok, but we don't know that (unless we check)
    println(someJavaClass.someNullableMethod().length) // may blow up
    println(someJavaClass.someOptionalMethod().get().length) // get without isPresent is bad enough already, may blow up
}