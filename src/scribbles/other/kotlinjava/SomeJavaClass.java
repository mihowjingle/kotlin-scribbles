package scribbles.other.kotlinjava;

import java.util.Optional;
import java.util.Random;

public class SomeJavaClass {

    private static final Random RANDOM = new Random();

    String someNotNullMethod() {
        return "string";
    }

    String someNullableMethod() {
        if (RANDOM.nextBoolean()) {
            return "string";
        } else {
            return null;
        }
    }

    Optional<String> someOptionalMethod() {
        if (RANDOM.nextBoolean()) {
            return Optional.of("string");
        }
        return Optional.empty();
    }
}
