package scribbles.other.kotlinjava

fun main() {
    val someKotlinWrapper = SomeKotlinWrapper()
    println(someKotlinWrapper.someNotNullMethod().length)
    println(someKotlinWrapper.someNullableMethod()?.length)
    println(someKotlinWrapper.someOptionalMethod()?.length)
}