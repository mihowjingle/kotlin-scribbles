package scribbles.other

import scribbles.lib.booleans.translate

fun Any?.isNull() = this == null // Kotlin, why are you so great? :D

fun main() {
    val x = null
    print("Is null null? ")
    println(x.isNull().translate("yes", "no"))
}