package scribbles.other

interface Observer<T> {
    fun update(newValue: T)
}

interface Observable<T> {
    fun register(observer: Observer<T>)
    fun unregister(observer: Observer<T>)
}

class Thermometer : Observable<Double> {

    var temperature = 0.0
        set(value) {
            notify(value)
            field = value
        }

    private val observers = mutableListOf<Observer<Double>>()

    override fun register(observer: Observer<Double>) {
        observers.add(observer)
    }

    override fun unregister(observer: Observer<Double>) {
        observers.remove(observer)
    }

    // in literature, it is supposed to be in the interface, I guess that's when you want to externally tell the Observable
    // to notify observers (or simply to force that it is implemented at all? dunno)
    // in my little variation here, the Observable itself knows to notify observers
    // of course it still has to notify them, it's just that now, this part of the pattern is not explicitly "documented" by a contract
    // ...can't tell if this is better or worse
    private fun notify(newValue: Double) {
        observers.forEach { it.update(newValue) }
    }
}

class TemperatureDisplay : Observer<Double> {
    override fun update(newValue: Double) {
        println("The temperature on the screen changes to: $newValue")
    }
}

class TemperatureVoiceover : Observer<Double> {
    override fun update(newValue: Double) {
        println("An artificial voice says: 'The temperature is now $newValue degrees'")
    }
}

class DatabaseClient : Observer<Double> {
    override fun update(newValue: Double) {
        println("INSERT INTO temperatures(temperature) VALUES ($newValue)")
    }
}

fun main() {
    val thermometer = Thermometer()
    val observable: Observable<Double> = thermometer

    observable.register(TemperatureDisplay())
    val voiceover = TemperatureVoiceover()
    observable.register(voiceover)

    thermometer.temperature = 0.9
    println("\n=-=-=-=-=-=-=-=-=-=-=\n")
    thermometer.temperature = 2.7
    observable.unregister(voiceover)
    println("\n=-=-=-=-=-=-=-=-=-=-=\n")
    thermometer.temperature = 4.5
    println("\n=-=-=-=-=-=-=-=-=-=-=\n")
    thermometer.register(DatabaseClient())
    thermometer.temperature = 2.3
}