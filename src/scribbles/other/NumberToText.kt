package scribbles.other

private val ROUND_NUMBERS = mapOf(
        // and so on... (order is important (doesn't have to be))
        // this is enough for Long, but we could go on for BigDecimal
        1_000_000_000_000_000_000L to "quintillion",
        1_000_000_000_000_000L to "quadrillion",
        1_000_000_000_000L to "trillion",
        1_000_000_000L to "billion",
        1_000_000L to "million",
        1_000L to "thousand"
)

private val DIRECT_TRANSFORMATIONS = mapOf(
        1L to "one",
        2L to "two",
        3L to "three",
        4L to "four",
        5L to "five",
        6L to "six",
        7L to "seven",
        8L to "eight",
        9L to "nine",
        10L to "ten",
        11L to "eleven",
        12L to "twelve",
        13L to "thirteen",
        14L to "fourteen",
        15L to "fifteen",
        16L to "sixteen",
        17L to "seventeen",
        18L to "eighteen",
        19L to "nineteen",
        20L to "twenty",
        30L to "thirty",
        40L to "forty",
        50L to "fifty",
        60L to "sixty",
        70L to "seventy",
        80L to "eighty",
        90L to "ninety"
)

fun numberToText(x: Long): String {
    if (x == 0L) {
        return "zero"
    }
    var result = ""
    for ((number, label) in ROUND_NUMBERS) {
        val numberOfRoundNumbers = x / number
        if (numberOfRoundNumbers % 1000 > 0) {
            result += belowThousand(numberOfRoundNumbers) + " " + label
            val remainder = x % number
            if (remainder > 0) {
                result += ", "
            }
        }
    }
    result += belowThousand(x)
    return result
}

private fun belowThousand(x: Long): String {
    var result = ""
    val howManyHundred = (x / 100) % 10
    val overOneHundred = howManyHundred > 0
    if (overOneHundred) {
        result += DIRECT_TRANSFORMATIONS[howManyHundred] + " hundred"
    }
    val remainderOfHundred = x % 100
    if (remainderOfHundred > 0) {
        if (overOneHundred) {
            result += " and "
        }
        result += belowHundred(remainderOfHundred)
    }
    return result
}

fun belowHundred(x: Long): String {
    if (x <= 20) {
        return toTextDirectly(x)
    } else {
        val ones = x % 10
        val tens = x - ones
        if (ones == 0L) {
            return toTextDirectly(tens)
        }
        return toTextDirectly(tens) + " " + toTextDirectly(ones)
    }
}

private fun toTextDirectly(x: Long) = DIRECT_TRANSFORMATIONS[x] ?: error("Hmm, should be impossible...")

fun main() { // if we're gonna make 'numberToText' a lib function, todo make these tests
    println(numberToText(5_289_999_209_232_322_327))
    println(numberToText(289_999_209_232_322_553))
    println(numberToText(999_209_232_327_323))
    println(numberToText(9_209_232_399_456))
    println(numberToText(8_232_322_323))
    println(numberToText(55_322_117))
    println(numberToText(72_873))
    println(numberToText(3_311))
    println(numberToText(623))
    println(numberToText(43))
    println(numberToText(5))
    println(numberToText(0))
    println(numberToText(10))
    println(numberToText(30))
    println(numberToText(60))
    println(numberToText(110))
    println(numberToText(200))
    println(numberToText(703))
    println(numberToText(1_000))
    println(numberToText(5_000))
    println(numberToText(10_000))
    println(numberToText(300_000))
    println(numberToText(4_000_000))
    println(numberToText(90_000_000))
    println(numberToText(90_000_001)) // or "ninety million and one"? probably todo check
    println(numberToText(19_000_000_000_000))
    println(numberToText(777))
}
