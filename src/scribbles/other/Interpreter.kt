package scribbles.other

import scribbles.lib.booleans.not
import scribbles.lib.functions.forever
import scribbles.lib.strings.collapse
import java.math.BigDecimal
import java.math.RoundingMode

fun take(accumulator: BigDecimal, x: BigDecimal) = x
fun add(accumulator: BigDecimal, x: BigDecimal) = accumulator + x
fun subtract(accumulator: BigDecimal, x: BigDecimal) = accumulator - x
fun multiplyBy(accumulator: BigDecimal, x: BigDecimal) = accumulator * x
fun divideBy(accumulator: BigDecimal, x: BigDecimal): BigDecimal = accumulator.divide(x, 20, RoundingMode.HALF_UP)

class Operation(val verb: (BigDecimal, BigDecimal) -> BigDecimal, val argument: BigDecimal) {
    companion object {
        fun fromString(string: String): Operation {
            val verbFragment = string.substringBeforeLast(delimiter = " ")
            val argumentFragment = string.substringAfterLast(delimiter = " ")
            return Operation(
                verb = possibleOperations[verbFragment] ?: throw IllegalArgumentException(),
                argument = argumentFragment.toBigDecimal()
            )
        }
    }
}

val possibleOperations = mapOf<String, (BigDecimal, BigDecimal) -> BigDecimal>(
        "take" to ::take,
        "add" to ::add,
        "subtract" to ::subtract,
        "multiply by" to ::multiplyBy,
        "divide by" to ::divideBy
)

// syntax:
// take x, add x, multiply by x, etc... where x's are numbers
// for example:
// take 0, add 3, subtract 5, multiply by -2 (prints 4)

fun main() {
    println("Welcome to super-duper math interpreter!")
    forever {
        print("-> ")
        val query = readLine() ?: return@forever // basically 'continue'
        if (not(query.startsWith("take "))) {
            println(" = Invalid syntax!")
            return@forever
        }
        val fragments = query.replace(",", ", ").collapse(' ').split(", ")
        val operations = try {
            fragments.map { Operation.fromString(it) }
        } catch (_: Exception) {
            println(" = Invalid syntax!")
            return@forever
        }
        val result = operations.fold(initial = BigDecimal.ZERO, operation = { acc: BigDecimal, operation: Operation -> operation.verb(acc, operation.argument) })
        println(" = ${result.stripTrailingZeros().toPlainString()}")
    }
}