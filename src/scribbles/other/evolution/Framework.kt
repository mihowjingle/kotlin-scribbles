package scribbles.other.evolution

// or "Environment"?
class Evolution<T>(randomSpecimen: () -> T, private val fitness: (T) -> Double, private val mutation: (T) -> T, populationSize: Int = 1000) {

    init {
        if (populationSize < 1) {
            error("The population must not be empty!")
        }
    }

    private var ppl = run {
        val p = mutableListOf<T>()
        repeat(times = populationSize) {
            p.add(randomSpecimen())
        }
        p
    }

    val population get() = ppl as List<T> // as immutable

    val best get() = ppl.maxByOrNull(fitness) ?: error("won't see this (see init block)")

    fun evolve(steps: Int = 1000) {
        repeat(times = steps) {
            step()
        }
    }

    private fun <E> List<E>.firstHalf() = slice(0 until size / 2)

    private fun step() {
        ppl = ppl.sortedByDescending(fitness).firstHalf().toMutableList()
        ppl.addAll(ppl.map(mutation))
    }
}