package scribbles.other.evolution

import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

// Is an evolution (genetic) algorithm good for finding the point where,
// it's on average the closest to all of (some other) point(s)?

// Answer: yes, but it's not the "center of mass" as you might expect
// (that's just the point at average x, average y)

data class Point(val x: Double, val y: Double) {
    fun distanceFrom(that: Point): Double {
        val xDistanceSquare = (this.x - that.x).pow(2)
        val yDistanceSquare = (this.y - that.y).pow(2)
        return sqrt(xDistanceSquare + yDistanceSquare)
    }
}

/**
 * The "fitness function" (or rather un-fitness)
 * 0.0 is perfect (but possible only with [points] being of size 1), in which case it's just that point.
 * In other words the distance between any [Point] and itself is zero. Duh... why did I write this comment? :D
 */
fun Point.avgDistanceFrom(points: Iterable<Point>) = points.map { it.distanceFrom(this) }.average()

/**
 * The "proliferation" / mutation function
 */
fun mutate(point: Point) = Point(
    x = point.x + Random.nextDouble(-1.0, 1.0),
    y = point.y + Random.nextDouble(-1.0, 1.0)
)

/**
 * For the initial population
 */
fun randomPoint() = Point(Random.nextDouble(50.0), Random.nextDouble(50.0))

/**
 * The points that the result point is supposed to be as close as possible to.
 */
val points = listOf(

//    Point(1.0, 11.0),
//    Point(5.3, 7.5),
//    Point(6.1, 3.3),
//    Point(2.7, 16.6),

//    Point(1.0, 1.0),
//    Point(11.0, 11.0),

    Point(1.0, 1.0),
    Point(11.0, 1.0),
    Point(1.0, 11.0),
    Point(11.0, 11.0),
)

fun main() {

    val arithmeticAvgPoint = Point(
        x = points.map { it.x }.average(),
        y = points.map { it.y }.average()
    )

    println("Arithmetic avg point: $arithmeticAvgPoint")

    fun Collection<Double>.geometricAvg(): Double {
        if (this.isEmpty()) {
            return Double.NaN
        }
        var m = 1.0
        for (element in this) {
            m *= element
        }
        return abs(m).pow(1.0 / size)
    }

    val geometricAvgPoint = Point(
        x = points.map { it.x }.geometricAvg(),
        y = points.map { it.y }.geometricAvg()
    )

    println("Geometric avg point: $geometricAvgPoint")

    fun Collection<Double>.harmonicAvg(): Double {
        if (this.isEmpty()) {
            return Double.NaN
        }
        val divider = sumOf { 1.0 / it }
        return size / divider
    }

    val harmonicAvgPoint = Point(
        x = points.map { it.x }.harmonicAvg(),
        y = points.map { it.y }.harmonicAvg()
    )

    println("Harmonic avg point: $harmonicAvgPoint")

    val e1 = Evolution(
        randomSpecimen = ::randomPoint,
        fitness = { point -> -point.avgDistanceFrom(points) },
        mutation = ::mutate
    )

    e1.evolve()
//    println(e1.population)
    println("Point as close as possible on average (min total distance): ${e1.best}")

    fun Point.totalDifferenceInDistancesFrom(points: Iterable<Point>): Double {
        val distances = points.map { it.distanceFrom(this) }
        val maxDistance = distances.maxOf { it }
        return distances.sumOf { maxDistance - it }
    }

    val e2 = Evolution(
        randomSpecimen = ::randomPoint,
        fitness = { point -> -point.totalDifferenceInDistancesFrom(points) },
        mutation = ::mutate
    )

    e2.evolve()
    println("Point with minimum total difference (variation?) in distances: ${e2.best}")
}