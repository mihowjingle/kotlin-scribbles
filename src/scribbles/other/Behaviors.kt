package scribbles.other

// ...so I saw this video, on YouTube, about using this kind of object composition
// as an alternative to inheritance

fun hiss() = println("hissss....")
fun meow() = println("meow")
fun bark() = println("woof woof")

fun slither() = println("slithering")
fun walk() = println("walking")

open class Animal(
    private val soundMakingBehavior: () -> Unit,
    private val movingBehavior: () -> Unit,
    private val name: String
) {
    fun makeSound() {
        print("$name says: ")
        soundMakingBehavior()
    }

    fun move() {
        print("$name is ")
        movingBehavior()
    }
}

fun main() {
    val snake = Animal(::hiss, ::slither, "Jake the Snake")
    println("SNAKE")
    snake.makeSound()
    snake.move()

    val cat = Animal(::meow, ::walk, "Puss")
    println("\nCAT")
    cat.makeSound()
    cat.move()

    val dog = Animal(::bark, ::walk, "Pooch")
    println("\nDOG")
    dog.makeSound()
    dog.move()

    val angryCat = Animal(::hiss, ::walk, "Tom")
    println("\nANGRY CAT")
    angryCat.makeSound()
    angryCat.move()

    println("\n=-=-=-=-=-=-=-=-=-=-=\n")

    // but one doesn't exclude the other, I guess
    class Snake(name: String) : Animal(::hiss, ::slither, name)
    class Dog(name: String) : Animal(::bark, ::walk, name)
    class Cat(name: String) : Animal(::meow, ::walk, name)

    val youKnowWho = Snake("Nagini")
    val rex = Dog("Rex")
    val jerry = Cat("Jerry")

    println("OTHER SNAKE")
    youKnowWho.makeSound()
    youKnowWho.move()

    println("\nOTHER DOG")
    rex.makeSound()
    rex.move()

    println("\nOTHER CAT")
    jerry.makeSound()
    jerry.move()
}