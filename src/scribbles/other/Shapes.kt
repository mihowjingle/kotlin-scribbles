package scribbles.other

import kotlin.math.PI
import kotlin.math.pow

interface Shape {
    val circumference: Double
    val area: Double
}

/**
 * @param sideLength vertical edge length
 * @param baseLength horizontal edge length
 */
open class Rectangle(val sideLength: Double, val baseLength: Double) : Shape {
    override val circumference = 2 * (sideLength + baseLength)
    override val area = baseLength * sideLength
}

class Square(sideLength: Double) : Rectangle(sideLength, sideLength)

class Circle(val radius: Double) : Shape {
    override val circumference = 2 * PI * radius
    override val area = PI * radius.pow(2)
    val diameter = 2 * radius
    override fun toString() =
        "Circle(radius=$radius, circumference=$circumference, area=$area, diameter=$diameter)"
}

fun main() { // todo tutorial

    val rectangle = Rectangle(sideLength = 2.0, baseLength = 3.0)
    val square = Square(2.0)
    val circle = Circle(radius = 1.0)
    val anonymousShape = object : Shape { // for example some irregular blob that we can't say much about, or whatever... it still has SOME circumference and area
        override val circumference = 7.0
        override val area = 5.5
    }

    println("What do we know about the rectangle?")
    println("Its side length is ${rectangle.sideLength}")
    println("Its base length is ${rectangle.baseLength}")
    println("Its circumference is ${rectangle.circumference}")
    println("Its area is ${rectangle.area}")

    print("\n\n")
    println("What do we know about the square?")
    println("Its side length is ${square.sideLength}")
    println("Its circumference is ${square.circumference}")
    println("Its area is ${square.area}")
    val omgWhatIsIt: Shape = square // so that we "don't know" and avoid warnings
    println("Is the square also a rectangle? ${omgWhatIsIt is Rectangle}")

    print("\n\n")
    println("What do we know about the circle?")
    println("Its radius is ${circle.radius}, so its diameter is, of course, twice that: ${circle.diameter}")
    println("Its circumference is ${circle.circumference}")
    println("Its area is ${circle.area}")
    println("Since we implemented 'toString' for Circle, we can just say: $circle (the circle knows how to describe itself)")

    print("\n\n")
    println("What do we know about the anonymous shape?")
    println("Well... only what it promises to tell us by being a Shape - its circumference: ${anonymousShape.circumference} and area: ${anonymousShape.area}")
    println("Which is exactly what we specified, so... nice to see that works :)")

    val shapes = arrayOf(square, rectangle, circle, anonymousShape)
    print("\n\n")
    println("Here are some of the more interesting things we can know about all the shapes together (and some options of how to ask):")
    println("There are ${shapes.size} of them.")
    println("Are they all circles? ${shapes.all { it is Circle }}")
    println("Is any of them a circle? ${shapes.any { shape -> shape is Circle }}")
    println("None of the shapes is a rectangle: ${shapes.none { it is Rectangle }}")
    println("How many are larger than 5, in terms of their area? ${shapes.count { shape -> shape.area > 5.0 }}")
    println("Shapes with circumference lesser than 9: ${shapes.filter(fun(shape) = shape.circumference < 9.0)}")
    println("Shapes with circumference greater than 9: ${shapes.filter { shape -> shape.circumference > 9.0 }}")
    println("A shape with circumference of exactly 9 (the first one that could be found, if one exists): ${shapes.find { shape -> shape.circumference == 9.0 }}")
    println("The sum of circumferences is ${shapes.fold(initial = 0.0, operation = { circumferenceSoFar, shape -> circumferenceSoFar + shape.circumference })}")
    println("The sum of areas is ${shapes.sumOf(Shape::area)}")
    println("The areas are ${shapes.map(Shape::area)}")
    println("The circumference of the shape with the largest area is ${shapes.maxByOrNull { it.area }?.circumference} (or null if there are no shapes, but we know there are)")
}