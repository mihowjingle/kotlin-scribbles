package scribbles.matchboxes

const val message = """
Hello, let's find out, how many "equations" made of matchsticks there are,
such that you can move exactly one matchstick for the equation to become correct!

...and learn Kotlin along the way.

(Assuming only one-cypher numbers and only addition or subtraction)
"""

fun main() {

    println(message)

    var count = 0
    for (left in Cypher.values())
        for (sign in Sign.values())
            for (right in Cypher.values())
                for (result in Cypher.values()) {
                    val equation = Equation(left, sign, right, result)
                    if (equation.incorrectButOneSwapFixesIt()) {
                        println(equation)
                        count++
                    }
                }

    println("\n$count equations (some may have more that one correct solution)!")
}