package scribbles.matchboxes

class Equation(private val pattern: Pattern) {

    constructor(left: Cypher, sign: Sign, right: Cypher, result: Cypher) :
            this(Pattern(left.pattern, sign.pattern, right.pattern, result.pattern))

    override fun toString(): String {
        val left = Cypher.of(pattern.left)
        val sign = Sign.of(pattern.sign)
        val right = Cypher.of(pattern.right)
        val result = Cypher.of(pattern.result)

        return if (valid()) {
            "$left $sign $right = $result"
        } else {
            "INVALID"
        }
    }

    private fun valid() = pattern.left.valid() && pattern.sign.valid() && pattern.right.valid() && pattern.result.valid()

    private fun correct(): Boolean {
        val left = Cypher.of(pattern.left)
        val sign = Sign.of(pattern.sign)
        val right = Cypher.of(pattern.right)
        val result = Cypher.of(pattern.result)

        return if (left != null && sign != null && right != null && result != null) {
            sign(left, right) == result.value
        } else {
            false
        }
    }

    fun incorrectButOneSwapFixesIt(): Boolean {

        if (correct()) return false

        val patternAsArray = flatten()

        for (i in 0 .. patternAsArray.lastIndex) {
            for (j in i+1 .. patternAsArray.lastIndex) {
                if (patternAsArray[i] == patternAsArray[j]) continue
                val newEquation = swapAndRebuildEquation(patternAsArray, i, j)
                if (newEquation.correct()) return true
            }
        }

        return false
    }

    private fun swapAndRebuildEquation(original: Array<MatchState>, i: Int, j: Int): Equation {
        val result = original.copyOf()
        result[i] = result[i].other()
        result[j] = result[j].other()
        return Equation(Pattern(result))
    }

    private fun flatten(): Array<MatchState> { // uh, yeah...
        return arrayOf(
            pattern.left.top,
            pattern.left.topLeft,
            pattern.left.topRight,
            pattern.left.middle,
            pattern.left.bottomLeft,
            pattern.left.bottomRight,
            pattern.left.bottom,

            pattern.sign.vertical,
            pattern.sign.horizontal,

            pattern.right.top,
            pattern.right.topLeft,
            pattern.right.topRight,
            pattern.right.middle,
            pattern.right.bottomLeft,
            pattern.right.bottomRight,
            pattern.right.bottom,

            pattern.result.top,
            pattern.result.topLeft,
            pattern.result.topRight,
            pattern.result.middle,
            pattern.result.bottomLeft,
            pattern.result.bottomRight,
            pattern.result.bottom
        )
    }

    data class Pattern(val left: Cypher.Pattern,
                       val sign: Sign.Pattern,
                       val right: Cypher.Pattern,
                       val result: Cypher.Pattern
    ) {
        constructor(array: Array<MatchState>) : this(
            Cypher.Pattern(array.sliceArray(0..6)),
            Sign.Pattern(array.sliceArray(7..8)),
            Cypher.Pattern(array.sliceArray(9..15)),
            Cypher.Pattern(array.sliceArray(16..22))
        )
    }
}