package scribbles.matchboxes

/**
 * Yes, I'm using the metaphor of matches in this program, (because I've seen these puzzles on matchboxes)
 * but here, it's more like a digital clock, hence DIM/LIT. Not that a match is on fire.
 */
enum class MatchState {
    LIT,
    DIM;

    fun other() = if (this == LIT) DIM else LIT
}