package scribbles.matchboxes

import scribbles.matchboxes.MatchState.DIM
import scribbles.matchboxes.MatchState.LIT

enum class Sign(val pattern: Pattern) {
    PLUS(Pattern(LIT, LIT)) {
        override fun toString() = "+"
        override operator fun invoke(a: Cypher, b: Cypher) = a.value + b.value
    },
    MINUS(Pattern(DIM, LIT)) {
        override fun toString() = "-"
        override operator fun invoke(a: Cypher, b: Cypher) = a.value - b.value
    };

    companion object {
        fun of(pattern: Pattern): Sign? = values().find { it.pattern == pattern }
    }

    abstract operator fun invoke(a: Cypher, b: Cypher): Int

    data class Pattern(val vertical: MatchState, val horizontal: MatchState) {
        constructor(array: Array<MatchState>) : this(array[0], array[1])
        fun valid() = of(this) != null
    }
}