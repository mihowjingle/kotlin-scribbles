package scribbles.matchboxes

import scribbles.matchboxes.MatchState.DIM
import scribbles.matchboxes.MatchState.LIT

enum class Cypher(val value: Int, val pattern: Pattern) {

    ZERO(0,     Pattern(LIT, LIT, LIT, DIM, LIT, LIT, LIT)),
    ONE(1,      Pattern(DIM, DIM, LIT, DIM, DIM, LIT, DIM)),
    TWO(2,      Pattern(LIT, DIM, LIT, LIT, LIT, DIM, LIT)),
    THREE(3,    Pattern(LIT, DIM, LIT, LIT, DIM, LIT, LIT)),
    FOUR(4,     Pattern(DIM, LIT, LIT, LIT, DIM, LIT, DIM)),
    FIVE(5,     Pattern(LIT, LIT, DIM, LIT, DIM, LIT, LIT)),
    SIX(6,      Pattern(LIT, LIT, DIM, LIT, LIT, LIT, LIT)),
    SEVEN(7,    Pattern(LIT, DIM, LIT, DIM, DIM, LIT, DIM)),
    EIGHT(8,    Pattern(LIT, LIT, LIT, LIT, LIT, LIT, LIT)),
    NINE(9,     Pattern(LIT, LIT, LIT, LIT, DIM, LIT, LIT));

    override fun toString(): String {
        return "$value"
    }

    companion object {
        fun of(pattern: Pattern): Cypher? = values().find { it.pattern == pattern }
    }

    data class Pattern(val top: MatchState,
                       val topLeft: MatchState,
                       val topRight: MatchState,
                       val middle: MatchState,
                       val bottomLeft: MatchState,
                       val bottomRight: MatchState,
                       val bottom: MatchState
    ) {
        constructor(array: Array<MatchState>) : this( // uh, yeah...
            array[0],
            array[1],
            array[2],
            array[3],
            array[4],
            array[5],
            array[6]
        )
        fun valid() = of(this) != null
    }
}
