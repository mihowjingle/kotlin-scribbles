package scribbles.projecteuler

// https://projecteuler.net/problem=9

// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
//
// a^2 + b^2 = c^2
// For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
//
// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
// Find the product abc.

fun pythagorean(a: Int, b: Int, c: Int) = a * a + b * b == c * c

fun main() {
    for (a in 1 until 1000) {
        for (b in a until 1000) {
            for (c in b until 1000) {
                if (pythagorean(a, b, c) && a + b + c == 1000) {
                    println("$a^2 + $b^2 = $c^2")
                    println("$a * $b * $c = ${a * b * c}")
                    return
                }
            }
        }
    }
}
