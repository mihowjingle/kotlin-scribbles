package scribbles.projecteuler

import scribbles.lib.numbers.isPrime

// https://projecteuler.net/problem=10

// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//
// Find the sum of all the primes below two million.

fun main() {
    var currentSum = 0L
    for (currentNumber in 0 until 2_000_000L) {
        if (currentNumber.isPrime()) {
            currentSum += currentNumber
        }
    }
    println("Sum of all primes until 2 million = $currentSum")
}