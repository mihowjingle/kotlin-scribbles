package scribbles.projecteuler

// https://projecteuler.net/problem=4

// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//
// Find the largest palindrome made from the product of two 3-digit numbers.

data class Multiplication(val x: Int, val y: Int) {

    val product = x * y

    fun productIsPalindrome(): Boolean {
        val string = product.toString()
        var left = 0
        var right = string.lastIndex
        while (left < right) {
            if (string[left] != string[right]) {
                return false
            }
            left++
            right--
        }
        return true
    }

    override fun toString() = "$x * $y = $product"
}

fun main() {
    val palindromes = mutableSetOf<Multiplication>()
    for (x in 999 downTo 100) {
        for (y in 999 downTo 100) {
            val multiplication = Multiplication(x, y)
            if (multiplication.productIsPalindrome()) {
                palindromes.add(multiplication)
            }
        }
    }
    println(palindromes.maxBy { it.product })
}