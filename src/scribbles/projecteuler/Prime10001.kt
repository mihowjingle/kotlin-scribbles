package scribbles.projecteuler

import scribbles.lib.numbers.isPrime

// https://projecteuler.net/problem=7

// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

// What is the 10 001st prime number?

fun nthPrimeNumber(n: Int): Long {
    var count = 0
    var current = 2L
    while (count < n) {
        if (current.isPrime()) {
            count++
        }
        current++
    }
    return current - 1
}

fun main() {
    println(nthPrimeNumber(n = 10001))
}
