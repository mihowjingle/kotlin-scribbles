package scribbles.projecteuler

import scribbles.lib.booleans.not
import scribbles.lib.functions.forever
import scribbles.lib.numbers.isPrime
import scribbles.lib.numbers.power

// https://projecteuler.net/problem=46

// It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.
//
// 9 = 7 + 2×1^2
// 15 = 7 + 2×2^2
// 21 = 3 + 2×3^2
// 25 = 7 + 2×3^2
// 27 = 19 + 2×2^2
// 33 = 31 + 2×1^2
//
// It turns out that the conjecture was false.
//
// What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?

fun Int.isComposite() = !this.isPrime() && this > 1

fun Int.isOdd() = this % 2 == 1

fun Int.isTwiceASquare(): Boolean {
    var n = 1 // is 0 twice a square? technically....
    forever {
        val p = 2 * n.power(2)
        if (p == this) {
            return true
        } else if (p > this) {
            return false
        }
        n++
    }
}

fun Int.closestSmallerPrime(): Int? {
    if (this <= 2) {
        return null
    }
    var maybePrime = this - 1
    while (maybePrime.isComposite()) {
        maybePrime--
        if (maybePrime < 2) {
            return null
        }
    }
    return maybePrime
}

fun Int.isPrimePlusTwiceASquare(): Boolean {
    var prime = this.closestSmallerPrime()
    while (prime != null) {
        if ((this - prime).isTwiceASquare()) {
            return true
        }
        prime = prime.closestSmallerPrime()
    }
    return false
}

fun main() {
    for (number in 0..Int.MAX_VALUE) {
        if (number.isComposite() && number.isOdd() && not(number.isPrimePlusTwiceASquare())) {
            println(number)
            break
        }
    }
}