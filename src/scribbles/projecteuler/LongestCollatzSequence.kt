package scribbles.projecteuler

// https://projecteuler.net/problem=14

// The following iterative sequence is defined for the set of positive integers:
//
// n → n/2 (n is even)
// n → 3n + 1 (n is odd)
//
// Using the rule above and starting with 13, we generate the following sequence:
//
// 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
// It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
// Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
//
// Which starting number, under one million, produces the longest chain?
//
// NOTE: Once the chain starts the terms are allowed to go above one million.

class CollatzSequence(private val startingNumber: Long) {

    private var count = 1
    private var currentNumber = startingNumber

    private fun Long.isEven() = this % 2 == 0L

    private fun next() {
        currentNumber = if (currentNumber.isEven()) {
            currentNumber / 2
        } else {
            3 * currentNumber + 1
        }
        count++
    }

    private fun compute(): CollatzSequence {
        while (currentNumber != 1L) {
            next()
        }
        return this
    }

    fun show() {
        println("Collatz sequence with starting number $startingNumber has $count terms.")
    }

    val length: Int get() {
        compute()
        return count
    }
}

fun main() {
    println("Longest Collatz sequence (1 <= starting number <= 1 000 000):")
    (1 until 1_000_000L)
        .map { CollatzSequence(it) }
        .maxBy { it.length }
        .show()
}